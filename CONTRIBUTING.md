# How to contribute

If you want to contribute to the project, we will ask you to follow some rules.  
Please, before pushing your code, take a read to this file as it can be modified during the project life.

## Rules

* Respect each branch context and follow a coherent branching model
* Try to make commits of small logical units
* Test your adds and put some meaningful comment to your commits
* Never push a not working program
* Keep your code _clean_ and try to be consistent to the global code style
* Use the `javadoc` comment style
* Use _camelCase_ convention for naming variables and functions, _PascalCase_ for classes and _CAPITAL\_UNDERSCORED_ for defines and constants
* Check that your adds are not conflicting with previous work. If you think that your idea is better, feel free to open an issue

## Submit changes

* Create a fork from the branch where you want to base your work
* Add your contribution
* Push your changes in your fork repository
* Submit a pull request to the main repository

## Documentation

Follow the `javadoc` style. That operation will permit the automatic generation of the code documentation.

## Getting Started

You can read some documents:
* [A successful git branching model](http://nvie.com/posts/a-successful-git-branching-model/)
* [Documenting C++ code](https://developer.lsst.io/docs/cpp_docs.html)
* [GitLab flow](https://about.gitlab.com/2014/09/29/gitlab-flow/)
