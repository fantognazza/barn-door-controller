# Barn Door Controller ![build status](https://gitlab.com/fantognazza/barn-door-controller/badges/master/build.svg)

This project consists in a generic controller code that pilots a barn door tracker (single or double arm).
The software was originally intended to cooperate with a companion app on smartphone, but any other integrations can also be taken into account.

Hardware must consist in a single stepper motor (e.g. NEMA 14) controlled by one of the following drivers: `A4988`, `DRV8825`, `DRV8834` or `DRV8880`.
  
The source code is built with the PlatformIO framework, so it is easily portable to other platforms and even compatible with Over-The-Air updates.  

All personalisations are contained in Configuration.h and BoardPinout.h, please adapt them according to your setup.

For now all tests are made on a `ATMEGA 328P` MCU (Arduino Pro Mini) with a `A4988` motor driver.


## Getting Started

Clone this repository on your computer and move inside it
```
git clone https://gitlab.com/fantognazza/barn-door-controller.git
cd barn-door-controller
```

## Prerequisites

* [Python 2.7](https://www.python.org/download/releases/2.7/)
* [PlatformIO](http://docs.platformio.org/en/latest/installation.html)

## Building

### Configuration

1. Adapt definitions in `BoardPinout.h` for your controller
2. Choose your configuration in `Configuration.h`
3. Check if your board is already listed in `platformio.ini`   
    If not, check the [compatibility list](https://platformio.org/boards) and try to add your equipment.  
    We are happy to accept any contribution which aim is to expand board compatibility.

### Compilation

Start compilation by executing the following command in the project directory
```
platformio run
```
If anything goes wrong, verbose the output adding the `-v` flag

### Flashing

Simply run the `upload` target
```
platformio run -t upload
```

### Bootloader flash

If your board does not respond correctly or you cannot upload the program, maybe the MCU bootloader is corrupted.  
On Arduino, you can flash the bootloader using another working arduino and Arduino IDE, flashing the sketch `ArduinoISP` and setting `Arduino as ISP` as programmer.  
More details can be easily found on internet.

## Library dependencies

External libraries will be automatically donwloaded by platformIO in `.piolibdeps` folder.

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for tag the releases.

## Author

* **Francesco Antognazza** - *Initial work* - @fantognazza

## License

This project is licensed under the GPLv3 License - see the [LICENSE.md](LICENSE.md) file for details
