#include "CC41A.h"

/**
 * Constructor
 * Wraps to the Bluetooth base class
 */
CC41A::CC41A(HardwareSerial *debugConnection, SoftwareSerial *cmdConnection)
:Bluetooth(debugConnection, cmdConnection)
{}

/**
 * Initial setup of the device:
 * - register MAC address
 * - set the BT name
 * - set PIN (if possible)
 * @return                  nothing
 */
void CC41A::setup()
{
    this->cleanInputBuffer();
        
    // gather BT MAC address
    String mac;
    this->cmdConnection->println("AT+ADDR?");
    mac = this->cmdConnection->readString();
    this->sanitizeResponse(&mac);
    mac.toCharArray(this->macAddress,MAC_ADDR_SIZE);
    #ifdef DEBUG
        this->debugConnection->println("BT MAC address: " + mac);
    #endif // DEBUG

    this->cleanInputBuffer();

    // blindly set the BT name (cannot retreive current name)
    this->cmdConnection->println("AT+NAME" + String(NAME));
    this->cleanInputBuffer();

    // after setting the name, a reset is required
    this->cmdConnection->println("AT+RESET");
    this->cleanInputBuffer();

    return;
}

/**
 * Reset all settings of the BT device
 * @return                      nothing
 */
void CC41A::factoryReset()
{
    #ifdef DEBUG
        this->debugConnection->println("Factory reset BT settings");
    #endif // DEBUG
    this->cmdConnection->println("AT+RENEW");
    this->cleanInputBuffer();

    // reset, as required by manual
    delay(500);
    this->cmdConnection->println("AT+RESET");
    this->cleanInputBuffer();

    return;
}

/**
 * Reboot the device
 * @return                      nothing
 */
void CC41A::reset()
{
    #ifdef DEBUG
        this->debugConnection->println("Resetting BT");
    #endif // DEBUG
    this->cmdConnection->println("AT+RESET");

    return;
}

/**
 * [DEBUG]
 * Prints the list of commands to the debug connection
 * @return                      nothing
 */
void CC41A::listCommands()
{
    #ifdef DEBUG
    this->debugConnection->println("Listing available commands");
    this->cmdConnection->println("AT+HELP?");

    delay(500);
    while (this->cmdConnection->available())
    {
        this->debugConnection->println(this->cmdConnection->readString());
    }
    #endif // DEBUG

    return;
}

/**
 * Sanitize response from BT module (usually AT+{cmd}=  and OK terminators)
 * @param               the string to sanitize
 * @return              nothing
 */
void CC41A::sanitizeResponse(String *response)
{
    response->replace("+ADDR=",""); // remove the +ADDR prefix
    response->replace("+NAME=",""); // remove all OK responses
    response->replace("OK\n",""); // remove the ending OK
    response->replace("\n",""); // remove all new lines
    
    return;
}