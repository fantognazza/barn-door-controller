/**
 * BLUETOOTH class
 * 
 * Parent class that implements vendor-agnostic functions and common variables used by children
 * 
 */

#ifndef BLUETOOTH_H
#define BLUETOOTH_H

    #include <stdint.h>
    #include <Arduino.h>
    #include "Configuration.h"
    // serial communication library
    #include <SoftwareSerial.h>

    #define MAC_ADDR_SIZE 15

    class Bluetooth {
        public:
            Bluetooth(HardwareSerial *debugConnection, SoftwareSerial *cmdConnection);
            virtual void setup();
            virtual void factoryReset();
            virtual void reset();
            virtual void listCommands();

        protected:
            HardwareSerial *debugConnection;
            SoftwareSerial *cmdConnection;
            char macAddress[MAC_ADDR_SIZE];
            void cleanInputBuffer();
    };

#endif // BLUETOOTH_H