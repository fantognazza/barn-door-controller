#include "BarnDoor.h"

/**
 * Costructor of class
 */
BarnDoor::BarnDoor()
{
    // setup code
    pinMode(ENDSTOP, INPUT); // external pull up needed
    _stepper = this->stepper; // copy reference so can be used by ISR routine

    // motor allocation
    #ifdef DRIVER_A4988
        stepper = new A4988(MOTOR_STEPS, DIR, STEP, EN, MS1, MS2, MS3);
    #endif // DRIVER_A4988
    #ifdef DRIVER_DRV8825
        stepper = new DRV8825(MOTOR_STEPS, DIR, STEP, EN, MS1, MS2, MS3);
    #endif // DRIVER_DRV8825
    #ifdef DRIVER_DRV8834
        stepper = new DRV8834(MOTOR_STEPS, DIR, STEP, EN, MS1, MS2);
    #endif // DRIVER_DRV8834
    #ifdef DRIVER_DRV8880
        stepper = new DRV8880(MOTOR_STEPS, DIR, STEP, EN, MS1, MS2);
    #endif // DRIVER_DRV8880

    // setting basic motor setting
    this->stepper->begin(HOME_SPEED,MICRO_STEPS);    // driver initialization
    this->stepper->stop();                           // resetting step counter
    this->stepper->disable();                        // detach power supply of stepper motor

    // check if the barndoor is already homed
    if (digitalRead(ENDSTOP) == LOW)
        _homed = true;
    else
        _homed = false;

    // attach interrupt routine to ENDSTOP pin
    attachInterrupt(digitalPinToInterrupt(ENDSTOP), endstopISR, CHANGE);

    return;
}

/**
 * Get the number of steps per turn
 * @return                      the number of steps in a complete turn
 */
short BarnDoor::getMotorSteps()
{
    return MOTOR_STEPS;
}

/**
 * Get the current value of substepping
 * @return                      the number of substeps per fullstep
 */
short BarnDoor::getMicroSteps()
{
    return this->stepper->getMicrostep();
}

/**
 * Set the value of substepping
 * @return                      nothing
 */
void BarnDoor::setMicroSteps(uint8_t microSteps)
{
    // check if the value is admissible
    if ((microSteps > 0) && (microSteps <= 16) && ((microSteps & (microSteps-1)) == 0))
    {
        // if is moving, stop it
        if (this->stepper->getCurrentState() == this->stepper->CRUISING)
        {
            this->stepper->stop();
            this->stepper->disable();
        }

        // set the value 
        this->stepper->setMicrostep(microSteps);
    }
    return;
}

/**
 * Move the barn door tracker to the starting position
 * @return                      nothing
 */
void BarnDoor::home()
{
    // if is moving, stop it
    if (this->stepper->getCurrentState() == this->stepper->CRUISING)
        this->stepper->stop();

    // reset to default speed
    this->stepper->setRPM(HOME_SPEED);

    // go to switch
    this->stepper->enable();
    while (!_homed)
        this->stepper->rotate(1);

    // move away from switch
    while (_homed)
        this->stepper->rotate(-1);

    // stand by
    this->stepper->stop();
    this->stepper->disable();

    return;
}

/**
 * Call stepper loop
 * @return                      the ETA of the movement
 */
unsigned BarnDoor::motorLoop()
{
    return this->stepper->nextAction();
}

/**
 * Stopping motor
 * @return                      nothing
 */
void BarnDoor::stop()
{
    this->stepper->stop();
    return;
}

/**
 * Disabling stepper motor to drop power consumption
 * @return                      nothing
 */
void BarnDoor::disable()
{
    this->stepper->stop();
    this->stepper->disable();
    return;
}

/**
 * move the barn door
 * @param rpm                       the Revolution Per Minute (>0)
 * @param dir                       the direction {-1;1}
 * @param turns                     the number of revolution to do (>=0)
 * @return                          nothing
 */
void BarnDoor::move(float rpm, short dir, float turns)
{
    // if is moving, stop it
    if (this->stepper->getCurrentState() == this->stepper->CRUISING)
        this->stepper->stop();

    // set new RPM and constant speed profile
    this->stepper->setRPM(rpm);
    this->stepper->setSpeedProfile(this->stepper->CONSTANT_SPEED);

    // start the movement with requested direction and turns
    this->stepper->enable();
    this->stepper->startRotate(turns*dir*360);

    return;
}

/**
 * Return true if the barn door is at the home position
 * @return                      TRUE is homed
 */
bool BarnDoor::isHomed()
{
    return _homed;
};

/**
 * Interrupt Service Routine called when the switch is pressed
 */
void endstopISR()
{
    // if endstop is entered, stop the motor and set the homed state
    if (digitalRead(ENDSTOP) == LOW)
    {
        _stepper->stop();
        _homed = true;
    }
    // else the endstop is leaved, so update the homed state
    else
    {
        _homed = false;
    }
    return;
}