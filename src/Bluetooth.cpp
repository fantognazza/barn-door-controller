#include "Bluetooth.h"

/**
 * Costructor of class
 */
Bluetooth::Bluetooth(HardwareSerial *debugConnection, SoftwareSerial *cmdConnection)
{
    this->debugConnection = debugConnection;
    this->cmdConnection = cmdConnection;
    return;
}

/**
 * Clean the input buffer, command response cannot be interpreted as user command
 * @return                  nothing
 */
void Bluetooth::cleanInputBuffer()
{
    while(this->cmdConnection->available() > 0)
        this->cmdConnection->read();
    return;
}