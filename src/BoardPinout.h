/**
 * BOARD PINOUT DEFINITIONS
 * 
 * Contains the definitions related to the electronic board in use.
 * Configuration mismatch can led to explosions, smoke, sparks and fire!
 */

#ifndef BOARD_PINOUT_H
#define BOARD_PINOUT_H

    #include "Configuration.h"

    // stepper driver digital pins
    #ifdef DRIVER_A4988
        #define DIR        9
        #define STEP       8
        #define EN         7
        #define MS1        10
        #define MS2        11
        #define MS3        12
    #endif // DRIVER_A4988

    #ifdef AUTO_ALIGNMENT
        // magnetometer I2C pins
        #define SDA        A4
        #define SCL        A5
    #endif // AUTO_ALIGNMENT

    // serial pins
    #define TX         4
    #define RX         5

    // misc defines
    #define ENDSTOP    2    // must be an interrupt pin. Further info at https://www.arduino.cc/reference/en/language/functions/external-interrupts/attachinterrupt/
    #define LED        13

    #ifdef BATTERY_MANAGEMENT
        /**
         * Voltage input
         * Remember to set up a proper voltage divider (and also a diode) matching the input logic level boundaries of your board!!
         * VMAX equals to the voltage at the maximum input (e.g. 5 for the 5V atmega in common arduinos)
         */
        #define VIN        A3
        #define VMAX       5
    // check if the battery management is correctly configured
    #if ! defined(VIN) || ! defined(VMAX) || ! defined(VTHRESHOLD)
        #error "BATTERY_MANAGEMENT feature is not correctly configured. Check VIN, VMAX and VTHRESHOLD definitions"
    #endif
    #endif // BATTERY_MANAGEMENT

#endif // BOARD_PINOUT_H