/**
 * CONFIGURATION FILE
 * 
 * Contains all the definitions and constants of the project (except the board pinouts).
 * 
 */

#ifndef CONFIGURATION_H
#define CONFIGURATION_H

    /**
     * Stepper motor configuration
     * Set-up according to your motor
     */
    #define MOTOR_STEPS      200
    #define MICRO_STEPS      16
    #define HOME_SPEED       60   // RPM
    #define MAX_RPM          60

    /**
     * Definition of the motor driver
     * Enable only one of the following
     */
    #define DRIVER_A4988
    //#define DRIVER_DRV8825
    //#define DRIVER_DRV8834
    //#define DRIVER_DRV8880

    #if (defined(DRIVER_A4988) + defined(DRIVER_DRV8825) + defined(DRIVER_DRV8834) + defined(DRIVER_DRV8880)) != 1
        #error "Multiple definition of stepper motors. Enable only one type"
    #endif

    /**
     * Battery management
     * Stops the operation when the input voltage goes down below a certain treshold.
     * Usefull to avoid battery u
                thisndercharge (and possibly damages).
     * 
     * Set the VIN pin in your board configuration to fully enable this feature.
     * Set the voltage threshold (VTHRESHOLD) for your system, referring to battery datasheet and your electrical configuration.
     */
    //#define BATTERY_MANAGEMENT
    #ifdef BATTERY_MANAGEMENT
        #define V_THRESHOLD      6.20     // 2S LiIon battery min voltage
    #endif // BATTERY_MANAGEMENT

    /**
     * Communication protocol
     * Choose either bluetooth or wired serial connection and the connection speed
     */
    #define BLUETOOTH
    //#define WIRED
    #define BAUDRATE    9600

    /**
     * Bluetooth settings
     * Define of the BT module
     * Customization of the device name and the pin required to connect
     */
    #ifdef BLUETOOTH
        #define NAME        "AstroTracker"
        #define CONN_PIN    "8462"

        /**
        * Definition of the BT module
        * Enable only one of the following
        */
        #define MODULE_CC41A
        #if (defined(MODULE_CC41A)) != 1
            #error "Multiple definition of BT modules. Enable only one type"
        #endif
    #endif // BLUETOOTH

    /**
     * Environment variables
     */
    #define DEBUG               // comment to save at least 700 B on data section and 2700 B on program section
    #define THREAD_LENGTH  300  // length (in mm) of the threaded rod. This parameter is a security check to avoid over extension

#endif // CONFIGURATION_H