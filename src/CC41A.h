/**
 * CC-41A BLUETOOTH MODULE CLASS
 * 
 * Extends the bluetooth class with module-dependent logic. Here can be implemented quirks and fixes
 * 
 */

#ifndef CC41A_H
#define CC41A_H
#include <Arduino.h>
#include "Bluetooth.h"

class CC41A : public Bluetooth
{
    public:
        CC41A(HardwareSerial *debugConnection, SoftwareSerial *cmdConnection);
        void setup() override;
        void factoryReset() override;
        void reset() override;
        void listCommands() override;
    private:
        void sanitizeResponse(String *response);
};

#endif // CC41A_H