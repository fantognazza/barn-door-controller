/**
 * MAIN ENTRY
 * 
 * Classic Arduino program with setup() and loop() functions.
 * 
 */

/**
 * Include section
 */
#ifdef ARDUINO_FRAMEWORK
#include <Arduino.h>

    // personalizations
    #include "BoardPinout.h"
    #include "Configuration.h"

    // mechanical entity
    #include "BarnDoor.h"

    // serial communication library
    #include <SoftwareSerial.h>

    #ifdef BLUETOOTH
        #ifdef MODULE_CC41A
            #include "CC41A.h"
        #endif // MODULE_CC41A
    #endif

    // parser library
    #include <CmdParser.hpp>

#endif // ARDUINO_FRAMEWORK



/**
 * Global variables
 */

// hardware entity
BarnDoor barnDoor;

// (secondary, software) command serial connection
SoftwareSerial cmdConnection(RX, TX);

// if bluetooth is used, than a bluetooth object must be declared and used
#ifdef BLUETOOTH
    Bluetooth *bt;
#endif // BLUETOOTH

// parser utility
CmdParser cmdParser;



/**
 * Text section
 */

/**
 * Establish all connections to sensors.
 * Initialize pin directions, class and global variables.
 */
void setup() {
    // setup LED
    pinMode(LED, OUTPUT);

    // try to open (primary) debug serial connection
    #ifdef DEBUG
    Serial.begin(9600);
    while (!Serial)
    {
        delay(100); //wait for serial port to connect
    }
        Serial.println("debug serial connection established");
    #endif // DEBUG

    // print barn door characteristics
    #ifdef DEBUG
        Serial.print("motor settings (motor steps, micro steps): (");
        Serial.print(barnDoor.getMotorSteps());
        Serial.print(", ");
        Serial.print(barnDoor.getMicroSteps());
        Serial.println(")");
    #endif // DEBUG

    #ifdef BATTERY_MANAGEMENT
    #ifdef DEBUG
        Serial.print("setup of VIN pin ");
        Serial.println(VIN);
    #endif // DEBUG
        pinMode(VIN, INPUT);
    #endif // BATTERY_MANAGEMENT

    // try to open (secondary, software) command serial connection
    // (wired or bluetooth)
    #ifdef DEBUG
        Serial.println("setup of command serial connection");
    #endif // DEBUG
    cmdConnection.begin(BAUDRATE);

    //setup BT if used
    #ifdef BLUETOOTH
        #ifdef DEBUG
            Serial.println("setup of BT");
        #endif // DEBUG
        #ifdef MODULE_CC41A
            bt = new CC41A(&Serial,&cmdConnection);
        #endif // MODULE_CC41A
        bt->setup();
    #endif // BLUETOOTH

    #ifdef DEBUG
        Serial.println("tuning parser settings");
    #endif // DEBUG
    // enable the "command key=value" parsing format
    cmdParser.setOptKeyValue(true);         // Use Key=Value with getValueFromKey/_P() by parser
    cmdParser.setOptSeperator(' ');         // Set command seperator
    cmdParser.setOptIgnoreQuote(true);      // Enable cmd strings with "" -> SET VAL "HALLO WORLD"

    #ifdef DEBUG
        Serial.println("end of setup");
    #endif // DEBUG
}

/**
 * Receive and parse all commands from serial connection (bluetooth or wired).
 * There are 3 kinds of commands:
 * - runtime commands (connection agnostic)
 * - BT specific commands (available only when BT connection is used)
 * - debug commands
 */
void loop() {
    String cmd;

    digitalWrite(LED, LOW);

    unsigned motorETA = barnDoor.motorLoop(); // call the stepper loop to move the motor

    /*
    * if motor is not used, disable it to save power and keep it cool
    * WARNING: if the camera is heavy, the motor could spin and put a
    * reverse current in the motor driver!
    */ 
    if (motorETA <= 0)
    {
        barnDoor.disable();
    }

    // if there's a command on serial
    if (cmdConnection.available())
    {
        #ifdef DEBUG
            Serial.println("serial data available");
        #endif // DEBUG

        // retreive the command
        cmd = cmdConnection.readString();
        #ifdef DEBUG
            Serial.print("Received string: ");
            Serial.println(cmd);
        #endif // DEBUG

        // convert command to string and remove quotes
        char command[cmd.length()-1];
        cmd = cmd.substring(1,cmd.length());
        cmd.toCharArray(command, cmd.length());

        digitalWrite(LED,HIGH); // turn LED on when parsing the command
        if (cmdParser.parseCmd(command) != CMDPARSER_ERROR)
        {
            #ifdef DEBUG
                Serial.print("Received command: \"");
                Serial.print(cmdParser.getCommand());
                Serial.print("\"; Number of parameters: ");
                Serial.print(cmdParser.getParamCount());
                Serial.print(": ");
                for (uint16_t i=1; i <= cmdParser.getParamCount();i++)
                {
                    Serial.print(cmdParser.getCmdParam(i));
                    Serial.print(", ");
                }
                Serial.println("");
            #endif // DEBUG


            /**
             * Runtime commands:
             * - CONFIG
             *   * MICROSTEPS
             * - MOVE
             *   * RPM
             *   * DIR
             *   * TURNS
             * - HOME
             * - STOP
             * - DISABLE
             */
            if (cmdParser.equalCommand("CONFIG"))
            {
                // retrive value from key microSteps and check for validity
                char *keyValue = cmdParser.getValueFromKey("MICROSTEPS");
                #ifdef DEBUG
                    Serial.print("value of MICROSTEPS: ");
                    Serial.println(keyValue);
                #endif // DEBUG

                if (keyValue != NULL)
                {
                    // try to cast the value to an unsigned int of 8 bit
                    uint8_t microSteps = (uint8_t)atoi(keyValue);

                    //check the validity of the value
                    if ((microSteps > 0) && (microSteps <= 16) && ((microSteps & (microSteps-1)) == 0))
                    {
                        #ifdef DEBUG
                            Serial.print("Setting microSteps as ");
                            Serial.println(microSteps);
                        #endif // DEBUG
                        barnDoor.setMicroSteps(microSteps);
                        cmdConnection.println("OK");
                    }
                    else
                    {
                        #ifdef DEBUG
                            Serial.println("Invalid value for key 'MICROSTEPS'");
                        #endif // DEBUG
                        cmdConnection.println("KO");
                    }
                }
                #ifdef DEBUG
                else
                    Serial.println("parsing error: MICROSTEPS is NULL");
                #endif // DEBUG
                
            }
            else if (cmdParser.equalCommand("MOVE"))
            {
                // gather all the required values
                char *rpmValue = cmdParser.getValueFromKey("RPM");
                char *dirValue = cmdParser.getValueFromKey("DIR");
                char *turnsValue = cmdParser.getValueFromKey("TURNS");
                #ifdef DEBUG
                    Serial.print("value of RPM: ");
                    Serial.print(rpmValue);
                    Serial.print("; value of DIR: ");
                    Serial.print(dirValue);
                    Serial.print("; value of TURNS: ");
                    Serial.println(turnsValue);
                #endif // DEBUG

                // check if all the values are not empty
                if (rpmValue != NULL && dirValue != NULL && turnsValue != NULL)
                {
                    // cast all the values
                    float rpm = (float)atof(rpmValue);
                    short dir = (short)atoi(dirValue);
                    float turns = (float)atof(turnsValue);

                    // check the validity of the values
                    if (rpm >= 0 && rpm <= MAX_RPM && (dir == 1 || dir == -1) && turnsValue >= 0)
                    {
                        #ifdef DEBUG
                            Serial.print("Moving motor at ");
                            Serial.print(rpm);
                            Serial.print(" rpm with direction ");
                            Serial.print(dir);
                            Serial.print(" for ");
                            Serial.print(turns);
                            Serial.println(" turns");
                        #endif // DEBUG
                        barnDoor.move(rpm, dir, turns);
                        cmdConnection.println("OK");
                    }
                    else
                    {
                        #ifdef DEBUG
                            Serial.println("Invalid value for some key");
                        #endif // DEBUG
                        cmdConnection.println("KO");
                    } 
                }
                #ifdef DEBUG
                else
                    Serial.println("parsing error: some key is NULL");
                #endif // DEBUG
            }
            else if (cmdParser.equalCommand("HOME"))
            {
                #ifdef DEBUG
                    Serial.println("going to home position");
                #endif // DEBUG
                barnDoor.home();                // blocking instruction!
                #ifdef DEBUG
                    Serial.println("homed");
                #endif // DEBUG
                cmdConnection.println("OK");
            }
            else if (cmdParser.equalCommand("STOP"))
            {
                barnDoor.stop();
                #ifdef DEBUG
                    Serial.println("stopped");
                #endif // DEBUG
                cmdConnection.println("OK");
            }
            else if (cmdParser.equalCommand("DISABLE"))
            {
                barnDoor.disable();
                #ifdef DEBUG
                    Serial.println("disabled");
                #endif // DEBUG
                cmdConnection.println("OK");
            }


            /**
             * BT device specific commands (wrapped to Bluetooth class):
             * - factory
             * - reset
             * - help (DEBUG)
             */
            #ifdef BLUETOOTH
            else if (cmdParser.equalCommand("FACTORY"))
            {
                bt->factoryReset();
                cmdConnection.println("OK");
            }
            else if (cmdParser.equalCommand("RESET"))
            {
                bt->reset();
                cmdConnection.println("OK");
            }
            // debug commands
            #ifdef DEBUG
            else if (cmdParser.equalCommand("HELP"))
            {
                bt->listCommands();
                cmdConnection.println("OK");
            }
            #endif // DEBUG
            #endif //BLUETOOTH


            /**
             * Debug commands:
             * - PING
             * - HOMED
             * - GETINPUTVOLTAGE (BATTERY_MANAGEMENT)
             */
            #ifdef DEBUG
            else if (cmdParser.equalCommand("PING"))
            {
                Serial.println("PONG");
                cmdConnection.println("PONG");
            }
            else if (cmdParser.equalCommand("HOMED"))
            {
                Serial.println(barnDoor.isHomed());
                cmdConnection.println(barnDoor.isHomed());
            }
            #ifdef BATTERY_MANAGEMENT
            else if (cmdParser.equalCommand("GETINPUTVOLTAGE"))
            {
                double voltage = analogRead(VIN);
                int length = snprintf(NULL, 0, "%f", voltage);
                char message[length+1];
                snprintf(message, "%f", voltage);
                Serial.print("Battery input voltage: ");
                Serial.println(voltage);
                Serial.println(voltage);
            }
            #endif // BATTERY_MANAGEMENT
            else
            {
                Serial.println("Command not executed");
                cmdConnection.println("KO");
            }
            #endif // DEBUG
        }
    }
}