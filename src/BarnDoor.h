/**
 * BARN DOOR class
 * 
 * Manage mechanical entity by handling movement requests and motor setup
 * 
 */

#ifndef BARNDOOR_H
#define BARNDOOR_H

    #include <stdint.h>
    #include "Configuration.h"
    #include "BoardPinout.h"
    #ifdef ARDUINO_FRAMEWORK
        #include <Arduino.h>
        #ifdef DRIVER_A4988
            #include <A4988.h>
        #endif // DRIVER_A4988
        #ifdef DRIVER_DRV8825
            #include <DRV8825.h>
        #endif // DRIVER_DRV8825
        #ifdef DRIVER_DRV8834
            #include <DRV8834.h>
        #endif // DRIVER_DRV8834
        #ifdef DRIVER_DRV8880
            #include <DRV8880.h>
        #endif // DRIVER_DRV8880
    #endif // ARDUINO_FRAMEWORK

    static volatile bool _homed;
    #ifdef DRIVER_A4988
        static A4988 *_stepper;  // ignore compiler warning, it will be used by the ISR but the compiler cannot know it
    #endif // DRIVER_A4988

    void endstopISR();

    class BarnDoor {
        public:
            BarnDoor();
            void home();
            void stop();
            void disable();
            void move(float rpm, short dir, float turns);
            unsigned motorLoop();
            bool isHomed();
            void setMicroSteps(uint8_t microSteps);
            short getMotorSteps();
            short getMicroSteps();

        private:
            #ifdef DRIVER_A4988
                A4988 *stepper;
            #endif // DRIVER_A4988
    };

#endif // BARNDOOR_H